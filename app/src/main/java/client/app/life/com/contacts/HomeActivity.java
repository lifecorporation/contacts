package client.app.life.com.contacts;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.gdata.client.Query;
import com.google.gdata.client.Service;
import com.google.gdata.client.authn.oauth.GoogleOAuthHelper;
import com.google.gdata.client.authn.oauth.GoogleOAuthParameters;
import com.google.gdata.client.authn.oauth.OAuthException;
import com.google.gdata.client.authn.oauth.OAuthHmacSha1Signer;
import com.google.gdata.client.authn.oauth.OAuthParameters;
import com.google.gdata.client.authn.oauth.OAuthSigner;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.Link;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.contacts.GroupMembershipInfo;
import com.google.gdata.data.extensions.Email;
import com.google.gdata.data.extensions.ExtendedProperty;
import com.google.gdata.data.extensions.Im;
import com.google.gdata.data.extensions.Name;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.NoLongerAvailableException;
import com.google.gdata.util.ServiceException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;

public class HomeActivity extends AppCompatActivity {

    private URL feedUrl;
    private static final String username="charalampos.kalogiannakis@x10y.com";
    private static final String pwd="n8sKcyp@";
    private ContactsService service;
    CoordinatorLayout clHome;

    private int REQUEST_AUTHORIZATION = 5;

    ContactsService myService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        clHome = (CoordinatorLayout) findViewById(R.id.clHome);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        String url = "https://www.google.com/m8/feeds/contacts/default/full";
        /*https://www.google.com/m8/feeds/contacts/{userEmail}/full/*/
        try {
            this.feedUrl = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        myService = new ContactsService("Contacts");

        // Configure sign-in to request the user's ID, email address, and basic profile. ID and
        // basic profile are included in DEFAULT_SIGN_IN.
        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();*/

        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        /*GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();*/

        //new GetTask().execute();
        pickUserAccount();
    }

    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    String mEmail = null; // Received from newChooseAccountIntent(); passed to getToken()
    String SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";

    /**
     * Attempts to retrieve the username.
     * If the account is not yet known, invoke the picker. Once the account is known,
     * start an instance of the AsyncTask to get the auth token and do work with it.
     */
    private void getUsername() {
        if (mEmail == null) {
            Log.i("mEmail", "null");
            pickUserAccount();
        } else {
            Log.i("mEmail", mEmail);
            if (isDeviceOnline()) {
                new GetUsernameTask(HomeActivity.this, mEmail, SCOPE).execute();
            } else {
                Toast.makeText(this, R.string.not_online, Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isDeviceOnline(){
        ConnectivityManager cm = (ConnectivityManager) ((Context)(HomeActivity.this)).getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected())
        {
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected())
        {
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected())
        {
            return true;
        }
        return false;
    }

    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            // Receiving a result from the AccountPicker
            if (resultCode == RESULT_OK) {
                mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                // With the account name acquired, go get the auth token
                getUsername();
            } else if (resultCode == RESULT_CANCELED) {
                // The account picker dialog closed without selecting an account.
                // Notify users that they must pick an account to proceed.
                Toast.makeText(this, R.string.pick_account, Toast.LENGTH_SHORT).show();
            }
        }else if (requestCode == REQUEST_AUTHORIZATION){

            /*String url = "https://www.google.com/m8/feeds/contacts/default/full";

            try {
                this.feedUrl = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            new GetTask().execute();*/

            try {
                printAllContacts(myService);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
        // Handle the result from exceptions
    }

    private class GetTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            service = new ContactsService("ContactsSample");
            try {
                service.setUserCredentials(username, pwd);
            } catch (AuthenticationException e) {
                e.printStackTrace();
            }
            try {
                queryEntries();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

    }

    private void queryEntries() throws IOException, ServiceException{
        Log.i("queryEntries", "enter");
        Query myQuery = new Query(feedUrl);
        myQuery.setMaxResults(50);
        myQuery.setStartIndex(1);
        myQuery.setStringCustomParameter("showdeleted", "false");
        myQuery.setStringCustomParameter("requirealldeleted", "false");
//      myQuery.setStringCustomParameter("sortorder", "ascending");
//      myQuery.setStringCustomParameter("orderby", "");


        try{
            ContactFeed resultFeed = (ContactFeed)this.service.query(myQuery, ContactFeed.class);
            for (ContactEntry entry : resultFeed.getEntries()) {
                printContact(entry);
            }
            System.err.println("Total: " + resultFeed.getEntries().size() + " entries found");

        }
        catch (NoLongerAvailableException ex) {
            System.err.println("Not all placehorders of deleted entries are available");
        }

    }
    private void printContact(ContactEntry contact) throws IOException, ServiceException{
        System.err.println("Id: " + contact.getId());
        if (contact.getTitle() != null)
            System.err.println("Contact name: " + contact.getTitle().getPlainText());
        else {
            System.err.println("Contact has no name");
        }

        System.err.println("Last updated: " + contact.getUpdated().toUiString());
        if (contact.hasDeleted()) {
            System.err.println("Deleted:");
        }

        //      ElementHelper.printContact(System.err, contact);

        Link photoLink = contact.getLink("http://schemas.google.com/contacts/2008/rel#photo", "image/*");
        if (photoLink.getEtag() != null) {
            Service.GDataRequest request = service.createLinkQueryRequest(photoLink);

            request.execute();
            InputStream in = request.getResponseStream();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            RandomAccessFile file = new RandomAccessFile("/tmp/" + contact.getSelfLink().getHref().substring(contact.getSelfLink().getHref().lastIndexOf('/') + 1), "rw");

            byte[] buffer = new byte[4096];
            for (int read = 0; (read = in.read(buffer)) != -1; )
                out.write(buffer, 0, read);
            file.write(out.toByteArray());
            file.close();
            in.close();
            request.end();
        }

        System.err.println("Photo link: " + photoLink.getHref());
        String photoEtag = photoLink.getEtag();
        System.err.println("  Photo ETag: " + (photoEtag != null ? photoEtag : "(No contact photo uploaded)"));

        System.err.println("Self link: " + contact.getSelfLink().getHref());
        System.err.println("Edit link: " + contact.getEditLink().getHref());
        System.err.println("ETag: " + contact.getEtag());
        System.err.println("-------------------------------------------\n");
    }


    String token = null;

    public class GetUsernameTask extends AsyncTask<Void, Void, Void> {
        Activity mActivity;
        String mScope;
        String mEmail;

        GetUsernameTask(Activity activity, String name, String scope) {
            this.mActivity = activity;
            this.mScope = scope;
            this.mEmail = name;
        }

        /**
         * Executes the asynchronous job. This runs when you call execute()
         * on the AsyncTask instance.
         */
        @Override
        protected Void doInBackground(Void... params) {
            try {
                token = fetchToken();
                if (token != null) {
                    Log.i("TOKEN",token);



                    try {
                        printAllContacts(myService);
                    } catch (ServiceException e) {
                        e.printStackTrace();
                    }

                    // **Insert the good stuff here.**
                    // Use the token to access the user's Google data.
                }
            } catch (IOException e) {
                // The fetchToken() method handles Google-specific exceptions,
                // so this indicates something went wrong at a higher level.
                // TIP: Check for network connectivity before starting the AsyncTask.
            }
            return null;
        }

        /**
         * Gets an authentication token from Google and handles any
         * GoogleAuthException that may occur.
         */
        protected String fetchToken() throws IOException {
            try {
                return GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
            } catch (UserRecoverableAuthException userRecoverableException) {
                // GooglePlayServices.apk is either old, disabled, or not present
                // so we need to show the user some UI in the activity to recover.
                //mActivity.handleException(userRecoverableException);
                mActivity.startActivityForResult(
                        userRecoverableException.getIntent(), REQUEST_AUTHORIZATION);
                Log.i("Exception", "UserRecoverableAuthException");
            } catch (GoogleAuthException fatalException) {
                // Some other type of unrecoverable exception has occurred.
                // Report and log the error as appropriate for your app.
                Log.i("Exception", "GoogleAuthException");
            }
            return null;
        }
    }

    public void printAllContacts(ContactsService myService)
            throws ServiceException, IOException {
        // Request the feed
        URL feedUrl = new URL("https://www.google.com/m8/feeds/contacts/default/full");

        OAuthSigner signer = new OAuthHmacSha1Signer();
        GoogleOAuthHelper oauthHelper = new GoogleOAuthHelper(signer);

        GoogleOAuthParameters oauthParameters = new GoogleOAuthParameters();
        //oauthParameters.setOAuthConsumerKey(consumerKey);
        oauthParameters.setOAuthToken(token);
        oauthParameters.setOAuthType(OAuthParameters.OAuthType.TWO_LEGGED_OAUTH);
        // read/write access to Contacts and Contact Groups
        oauthParameters.setScope("https://www.google.com/m8/feeds/");
        oauthParameters.setOAuthConsumerKey("33621223565-84tpgoqd61tff131le0hg0t7jcvo7f3h.apps.googleusercontent.com");
        //oauthParameters.setOAuthConsumerSecret();
        try {
            myService.setOAuthCredentials(oauthParameters, signer);
        } catch (OAuthException e) {
            e.printStackTrace();
        }

        Query query = new Query(new URL(feedUrl + "?xoauth_requestor_id=" +
                this.mEmail));

        ContactFeed resultFeed = myService.query(query, ContactFeed.class);
        // Print the results
        System.out.println(resultFeed.getTitle().getPlainText());
        Log.i("printAllContacts", resultFeed.getEntries().toString());
        for (ContactEntry entry : resultFeed.getEntries()) {
            if (entry.hasName()) {
                Name name = entry.getName();
                if (name.hasFullName()) {
                    String fullNameToDisplay = name.getFullName().getValue();
                    if (name.getFullName().hasYomi()) {
                        fullNameToDisplay += " (" + name.getFullName().getYomi() + ")";
                    }
                    System.out.println("\t\t" + fullNameToDisplay);
                } else {
                    System.out.println("\t\t (no full name found)");
                }
                if (name.hasNamePrefix()) {
                    System.out.println("\t\t" + name.getNamePrefix().getValue());
                } else {
                    System.out.println("\t\t (no name prefix found)");
                }
                if (name.hasGivenName()) {
                    String givenNameToDisplay = name.getGivenName().getValue();
                    if (name.getGivenName().hasYomi()) {
                        givenNameToDisplay += " (" + name.getGivenName().getYomi() + ")";
                    }
                    System.out.println("\t\t" + givenNameToDisplay);
                } else {
                    System.out.println("\t\t (no given name found)");
                }
                if (name.hasAdditionalName()) {
                    String additionalNameToDisplay = name.getAdditionalName().getValue();
                    if (name.getAdditionalName().hasYomi()) {
                        additionalNameToDisplay += " (" + name.getAdditionalName().getYomi() + ")";
                    }
                    System.out.println("\t\t" + additionalNameToDisplay);
                } else {
                    System.out.println("\t\t (no additional name found)");
                }
                if (name.hasFamilyName()) {
                    String familyNameToDisplay = name.getFamilyName().getValue();
                    if (name.getFamilyName().hasYomi()) {
                        familyNameToDisplay += " (" + name.getFamilyName().getYomi() + ")";
                    }
                    System.out.println("\t\t" + familyNameToDisplay);
                } else {
                    System.out.println("\t\t (no family name found)");
                }
                if (name.hasNameSuffix()) {
                    System.out.println("\t\t" + name.getNameSuffix().getValue());
                } else {
                    System.out.println("\t\t (no name suffix found)");
                }
            } else {
                System.out.println("\t (no name found)");
            }
            System.out.println("Email addresses:");
            for (Email email : entry.getEmailAddresses()) {
                System.out.print(" " + email.getAddress());
                if (email.getRel() != null) {
                    System.out.print(" rel:" + email.getRel());
                }
                if (email.getLabel() != null) {
                    System.out.print(" label:" + email.getLabel());
                }
                if (email.getPrimary()) {
                    System.out.print(" (primary) ");
                }
                System.out.print("\n");
            }
            System.out.println("IM addresses:");
            for (Im im : entry.getImAddresses()) {
                System.out.print(" " + im.getAddress());
                if (im.getLabel() != null) {
                    System.out.print(" label:" + im.getLabel());
                }
                if (im.getRel() != null) {
                    System.out.print(" rel:" + im.getRel());
                }
                if (im.getProtocol() != null) {
                    System.out.print(" protocol:" + im.getProtocol());
                }
                if (im.getPrimary()) {
                    System.out.print(" (primary) ");
                }
                System.out.print("\n");
            }
            System.out.println("Groups:");
            for (GroupMembershipInfo group : entry.getGroupMembershipInfos()) {
                String groupHref = group.getHref();
                System.out.println("  Id: " + groupHref);
            }
            System.out.println("Extended Properties:");
            for (ExtendedProperty property : entry.getExtendedProperties()) {
                if (property.getValue() != null) {
                    System.out.println("  " + property.getName() + "(value) = " +
                            property.getValue());
                } else if (property.getXmlBlob() != null) {
                    System.out.println("  " + property.getName() + "(xmlBlob)= " +
                            property.getXmlBlob().getBlob());
                }
            }
            Link photoLink = entry.getContactPhotoLink();
            String photoLinkHref = photoLink.getHref();
            System.out.println("Photo Link: " + photoLinkHref);
            if (photoLink.getEtag() != null) {
                System.out.println("Contact Photo's ETag: " + photoLink.getEtag());
            }
            System.out.println("Contact's ETag: " + entry.getEtag());
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    public void onGroupItemClick(MenuItem item) {
        // One of the group items (using the onClick attribute) was clicked
        // The item parameter passed here indicates which item it is
        // All other menu item clicks are handled by onOptionsItemSelected()
        if (item.isChecked()) {
            item.setChecked(false);
        } else {
            item.setChecked(true);
        }
    }
}
